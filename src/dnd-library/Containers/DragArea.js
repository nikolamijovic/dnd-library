import React, { useState } from 'react';
import {objectCompare} from "../../Utils/objectCompare";
import PropTypes from 'prop-types';

// declare deep comparison function for objects
Object.compare = objectCompare;

export const DragArea = (props) => {

    const [ dragged, setDragged ] = useState(null);
    const [ transparent, setTransparent ] = useState(null);
    const { data, onChange } = props;

    const onDragStart = (e, i) => {
        // store the reference temporarily
        setDragged(data[i]);
        // set the effect for the moved item to ensure correct behaviour
        e.dataTransfer.effectAllowed = "move";
        // Firefox effect
        e.dataTransfer.setData("text/html", e.target.parentNode);
        // Chrome effect
        e.dataTransfer.setDragImage(e.target.parentNode, 20, 20);
    };

    const onDragOver = i => {
        // reference the element in state
        const draggedOver = data[i];
        // ignore passing over the dragged item itself
        if (Object.compare(dragged, draggedOver)) {
            return;
        }
        // get all other items
        let items = data.filter(item => Object.compare(item, dragged) === false);
        // add the dragged item after the one it was dragged over
        items.splice(i, 0, dragged);
        // set the index for transparency
        setTransparent(i);
        // rebuild state with the new position
        onChange(items);
    };

    const onDragEnd = () => {
        // cancel out the dragged item
        setDragged(null);
        // cancel out transparency
        setTransparent(null);
    };

    const children = React.Children.map(props.children, (child, index) => {
        return React.cloneElement(child, {
            onDragStart: onDragStart,
            onDragOver: onDragOver,
            onDragEnd: onDragEnd,
            data: data[index],
            transparent
        });
    });

    return (
        <div>
            {children}
        </div>
    );
};

DragArea.propTypes = {
    data: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};