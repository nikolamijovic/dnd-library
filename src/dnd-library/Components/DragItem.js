import React from 'react';
import PropTypes from 'prop-types';
import burger from '../Assets/burger.svg'
import classNames from 'classnames';
import styles from '../Styles/DragItem.module.scss';

const DragItem = (props) => {
    const {index, key, transparent, onDragStart, onDragOver, onDragEnd} = props;
    return (
        <div
            key={key}
            onDragOver={() => onDragOver(index)}
            className={
                classNames(
                    styles.dragItem, ((transparent === index) && styles.transparent),
                    (transparent && (index === transparent + 1)) && styles.marginBefore,
                    (transparent && (index === transparent - 1)) && styles.marginAfter
                )
            }>
            <div
                draggable
                onDragStart={e => onDragStart(e, index)}
                onDragEnd={() => onDragEnd()}
                className={styles.draggable}
            >
                {props.children}
                <img alt="drag" src={burger}/>
            </div>
        </div>
    );
};

DragItem.propTypes = {
    index: PropTypes.number.isRequired,
    transparent: PropTypes.number,
    onDragStart: PropTypes.func,
    onDragOver: PropTypes.func,
    onDragEnd: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};

export default DragItem;