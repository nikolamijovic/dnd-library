export const users = [
    {name: "Marcus Miller", email: "marcus@gmail.com"},
    {name: "John Petrucci", email: "john154@outlook.com"},
    {name: "Steve Vai", email: "stevan@zoho.com"},
    {name: "Joe Satriani", email: "joxx@yahoo.com"},
    {name: "Phil Collins", email: "drummer101@gmail.com"},
    {name: "Ola Englund", email: "broola@live.com"},
    {name: "Devin Townsend", email: "empath@outlook.com"},
    {name: "Mark Letierri", email: "markgtr@yahoo.com"},
];