import React, { useState } from 'react';
import {users} from "../Constants/users";
import {DragArea} from "../dnd-library/Containers/DragArea";
import DragItem from "../dnd-library/Components/DragItem";
import styles from '../Styles/DraggableUserList.module.scss'

export const DraggableUserList = () => {
    const [ exampleUsers, setExampleUsers ] = useState(users);

    return (
        <div className={styles.draggableList}>
            <ul>
                <DragArea data={exampleUsers} onChange={setExampleUsers}>
                    {exampleUsers.map((user, i) => {
                        return (
                            <DragItem key={user.email} index={i}>
                                <li className={styles.listItem}>
                                    <span>{user.name}</span>
                                    <span>{user.email}</span>
                                </li>
                            </DragItem>
                        )
                    })}
                </DragArea>
            </ul>
        </div>
    );
};