import React, { Component } from 'react';
import styles from './Styles/App.module.scss';
import {DraggableUserList} from "./Containers/DraggableUserList";

class App extends Component {
  render() {
    return (
      <div className={styles.appContainer}>
        <div className={styles.versioning}>
          <h1>React <span>dnd Library</span></h1>
          <h3>v.0.0.1</h3>
        </div>
        <DraggableUserList/>
      </div>
    );
  }
}

export default App;
